local function RandStr(str)
    local n = ""
    for i=1,#str do
        local c = ""
        local function g()
            local p = math.random(1,#str)
            local valid = true
            for i=1,#n do
                if n:sub(i,i) == string.sub(str,p,p) then
                    valid = false
                end
            end
            return valid and string.sub(str,p,p) or nil
        end
        while c == "" do
            local tc = g()
            if tc then
                c = tc
            end
        end
        n = n .. c
    end
    return n ~= "" and n or str
end

local function clamp(a,b,c)
    return a < b and b or a > c and c or a
end

local function noise(a,b)
    local o = ((b or 0)%50)+((a or 0)%25)
    o = (o - 0.5) / 50 
    return clamp(o-.5,-0.5,0.5)
end

local function targ(a,b,c)
    return ((a^b)*c) % (64 / 144)
end

local tos,ton,mf,ms,mp = tostring,tonumber,math.floor,math.sin,math.pi
local CONSTANTS = {
    0x093D975,0xEDB2DB8,0x401522F,
    0x7100689,0x992A750,0x9CC84EB,
    0xFE8273D,0x3CDFF96,0x44D061A,
    0x00E449B,0xDB73B77,0xFB196B9,
    0x95236C8,0xFC52A71,0xB82F2EB,
    0x461FA2E,0xD932D72,0x8B649AC,
    0xB715D08,0xA61FE85,0x75D574F,
    0x4B93CD6,0x8E0F908,0x3DAA3D1
}

return function(str)
    local tb = 0
    for i=1,#str do
        tb = tb .. string.byte(string.sub(str,i,i))
    end
    tb = tb * (noise(tb,#str) ^ 1)
    local cmap = ""
    for i=50,120 do
        if i ~= 39 and i ~= 34 then
            cmap = cmap .. string.char(i)
        end
    end
    for i=1,tb/(tb/2) do
        math.randomseed(tb*i)
        cmap = RandStr(cmap)
    end
    local res = ""
    local lc = "?"
    for i2=1,3 do
        for i=1,#CONSTANTS do
            local tbi = clamp(i,1,#tos(tb))
            local ci = (tos(tb):sub(tbi,tbi))
            ci = mf(ton(ci) or 1)
            ci = (CONSTANTS[i] * ci) * tbi
            ci = (ton(tos(ci):sub(1,1)) + (ton(tos(ci):sub(1,2))/mp)) + mp
            ci = (ci < 10 and ci * 3) or (ci > 45 and ci / 1.75) or ci
            ci = (ci < 1 and clamp(i,1,#tos(tb))) or (ci > #cmap and #cmap - ci/ms(clamp(i,1,#tos(tb)))) or ci
            ci = ci * (noise(ci*tbi,(#res+#str))+.5)
            ci = ci + targ(i2,i,#str)
            if cmap:sub(ci,ci) == lc then
                ci = ci + clamp(ci * (noise(string.byte(lc)*CONSTANTS[i],ci*tbi)) * mp ^ 2,1,#cmap) 
            end
            if ci < 1 then ci = 1 elseif ci > #cmap then ci = clamp(#cmap - (noise(#cmap*tbi,tb)*mp),1,#cmap) end
            res = res .. cmap:sub(ci,ci)
            lc = cmap:sub(ci,ci)
        end
    end
    return res
end